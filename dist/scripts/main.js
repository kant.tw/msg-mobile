$(function() {

	$('.acchead').each(function() {
		console.log('test ' + $(this).next('ul'));
		if ($(this).next('ul').length === 0) {
			$(this).removeClass('acchead');
		}
		
	});

	$('#menuTrigger').on('click', function(e) {
		e.preventDefault();
		$('#mainmenu').addClass('opened');
	});
	$('body').on('click', function(e) {
		if (($(e.target).parents('#mainmenu').length === 0) && ($(e.target).attr('id') !== 'menuTrigger')) {
			$('#mainmenu').removeClass('opened');
		}
	});

	$('#searchTrigger').on('click', function(e) {
		e.preventDefault();
		$('.searchInner').toggleClass('opened');
		$('.pageWrap_cover').toggleClass('opened');
	});

	

	var offCanvas = {
		init: function() {
			$('.acchead').each(function() {
				var target = $($(this).data('target'));
			});
			this.bindEvents();
		},

		clickTrigger: function() {
			$('body').on('click', '.acchead', function(e) {
				e.preventDefault();
				var targetSubmenu = $($(e.target).data('target'));
				
				if ($(e.target).hasClass('opened')) { //if opened, close it					
					targetSubmenu.removeClass('opened');
					$(e.target).removeClass('opened');
				} else { //open it and close others
					
					targetSubmenu.addClass('opened');
					$(e.target).addClass('opened');
					var siblings = $(e.target).parent('li').siblings().find('.acchead');
					siblings.removeClass('opened');
					siblings.next('ul').removeClass('opened');
				}
			})
		},

		bindEvents: function() {
			this.clickTrigger();
		}
	}

	offCanvas.init();


	$('.banner').slick({
		autoplay: true,
		dots: true,
		arrows: false
	});

	//***************** c-tabs *****************//
	var tabs = function(options) {

	    var el = document.querySelector(options.el);
	    var tabNavigationLinks = el.querySelectorAll(options.tabNavigationLinks);
	    var tabContentContainers = el.querySelectorAll(options.tabContentContainers);
	    var activeIndex = 0;
	    var initCalled = false;

	    var init = function() {
	    	if (!initCalled) {
			    initCalled = true;
			    el.classList.remove('no-js');

			    tabNavigationLinks[0].className += ' is-active';
			    for (var i = 0; i < tabNavigationLinks.length; i++) {
			    	var link = tabNavigationLinks[i];
			    	handleClick(link, i);
			    }

			    tabContentContainers[0].className += ' is-active';
			}
	    };

	    var handleClick = function(link, index) {
	    	link.addEventListener('click', function(e) {
			    e.preventDefault();
			    goToTab(index);
			});
	    };

	    var goToTab = function(index) {
	    	if (index !== activeIndex && index >= 0 && index <= tabNavigationLinks.length) {
				tabNavigationLinks[activeIndex].classList.remove('is-active');
				tabNavigationLinks[index].classList.add('is-active');
				tabContentContainers[activeIndex].classList.remove('is-active');
				tabContentContainers[index].classList.add('is-active');
				activeIndex = index;
			}
	    };

	    return {
	    	init: init,
	    	goToTab: goToTab
	    };

	};

	window.tabs = tabs;
	//***************** end c-tabs *****************//
	
});
