define(function(require) {

    var pdContentUI = require('./pdcontent_ui');

    function buildHTML(imgs) {
        var finalHTML = '';
                                
        for (var colorkey in imgs) {
            var colorsetHTML = '<div class="pdcnt_slide"><div class="js-pdcntSlide">';
            // var mainImgHTML = '<div class="pdcontent_center_img js-pdMainSlide"><img src="" alt=""></div>';

            imgs[colorkey].forEach(function(colorimg) {
                colorsetHTML = colorsetHTML + '<img src="' + colorimg + '" />';

            });
            colorsetHTML = colorsetHTML + '</div>'
             + '<div class="js-pdcntSlide_nav"></div>'
             + '</div>';
            finalHTML = finalHTML + colorsetHTML;
        }

        $('.pdcnt_sliderwrap').append(finalHTML);
        
        new pdContentUI({
            el: '#pdcnt_wrap', 
            mainpic: '.pdcnt_slide', 
            color: '.pdcnt_color a',
            size: '.pdcnt_size',
            number: '.js-number',
            input_color: '#pdvalue_color',
            input_size: '#pdvalue_size',
            input_number: '#pdvalue_number',
            // btnWrap: '#pdcnt_btn',
            arrivedNotice: '#pdarrived_btn',
            arriveNoticePop: '#pdarrived_popup',
            arriveNoticePop_size: '#pdarrived-size',
            arriveNoticePop_color: '#pdarrived_popup_color',
            colorName: '.pdcnt_subtitle'
        }).init();


    }

    return buildHTML;
})
