define(['slick'], function(slick) {

	//pdcontent interaction
	function pdContentUI(options) {
		this.el = $(options.el);
		this.mainPics = this.el.find(options.mainpic);
		this.colorTrigger = this.el.find(options.color);
		this.sizeSelector = this.el.find(options.size);
		this.numberInput = this.el.find(options.number);
		this.input_color = $(options.input_color);
		this.input_size = $(options.input_size);
		this.input_number = $(options.input_number);
		// this.btnWrap = $(options.btnWrap);
		this.pdarrived_size = $(options.arriveNoticePop_size);
		this.pdarrived_color = $(options.arriveNoticePop_color);
		this.colorName = $(options.colorName);
		
		this.init = function() {
			this.mainPics.eq(0).addClass('active');

			this.mainPics.eq(0).find('.js-pdcntSlide').slick({
				dots: true,
				lazyLoad: 'progressive',
				adaptiveHeight: true,
				mobileFirst: true,
				appendDots: this.mainPics.eq(0).find('.js-pdcntSlide_nav')
			});
			$(window).trigger('resize'); //to fix mobile no image showed initially

			this.colorTrigger.eq(0).addClass('active');

			this.sizeSelector.each(function() {
				$(this).val($(this).find('option').eq(0).val());
			});
			this.sizeSelector.eq(0).addClass('active');

			this.handleNumber(this.sizeSelector.eq(0).find('option').eq(0).data('qty'));
			
			this.insertValues({
				color: this.colorTrigger.eq(0).data('color'), 
				size: this.sizeSelector.eq(0).find('option').eq(0).val(), 
				number: 1
			});

			// this.colorName.text(this.colorTrigger.eq(0).data('color'));

			this.bindevents();
			
		}
			
	}

	pdContentUI.prototype.colorClick = function() {
		var self = this;
		this.colorTrigger.on('click', function(e) {
			e.preventDefault();
			var targetIdx = $(this).index();
			
			//handle color
			self.colorTrigger.removeClass('active');
			$(this).addClass('active');

			//handle main pic
			self.mainPics.removeClass('active');
			self.mainPics.eq(targetIdx).addClass('active');
			if (!self.mainPics.eq(targetIdx).find('.js-pdcntSlide').hasClass('slick-initialized')) {
				self.mainPics.eq(targetIdx).find('.js-pdcntSlide').slick({
    				dots: true,
    				adaptiveHeight: true,
    				appendDots: self.mainPics.eq(targetIdx).find('.js-pdcntSlide').siblings('.js-pdcntSlide_nav')
    			});
			}

			//handle size selector
			self.sizeSelector.removeClass('active');
			self.sizeSelector.eq(targetIdx).addClass('active');

			self.handleNumber(self.sizeSelector.eq(targetIdx).find('option:selected').data('qty'));
			var insertnumber = (self.sizeSelector.eq(targetIdx).find('option:selected').val() === 0)? 0 : 1;
			self.insertValues({
				color: $(this).data('color'), 
				size: self.sizeSelector.eq(targetIdx).val(),
				number: insertnumber
			});

			self.colorName.text($(this).data('color'));
		});
		
	}

	pdContentUI.prototype.sizeChange = function() {
		var self = this;
		this.sizeSelector.on('change', function(e) {
			var newQty = $(e.target).find('option:selected').data('qty');
			
			self.handleNumber(newQty);
			var insertnumber = (newQty === 0)? 0 : 1;
			self.insertValues({size: $(this).val(), number: insertnumber});
		});
	}


	pdContentUI.prototype.numberChange = function() {
		var self = this;
		this.numberInput.on('change', function() {
			self.insertValues({number: $(this).val()});
		});
	}

	pdContentUI.prototype.handleNumber = function(currentQty) {
		var self = this;
		self.numberInput.empty();
		if (currentQty > 0) {
			// console.log('not empty: ' + currentQty);
			var optionHTML = '';
			for (var i = 1; i <= currentQty; i++) {
				optionHTML = optionHTML + '<option value="' + i + '">' + i + '</option>';
			}
			self.numberInput.append(optionHTML);
			self.numberInput.val(1);
			self.numberInput.attr('disabled', false);
			self.el.removeClass('qty0');
		} else { //if qty is 0
			// console.log('empty: ' + currentQty);
			self.numberInput.append('<option value="0">0</option>').val(0);
			self.numberInput.attr('disabled', true);
			self.el.addClass('qty0');
		}
	}

	pdContentUI.prototype.bindevents = function() {
		this.colorClick();
		this.sizeChange();
		this.numberChange();
	}

	pdContentUI.prototype.filledNumberOptions = function(qty) {

	}

	pdContentUI.prototype.insertValues = function(values) {
		var colorVal, sizeVal, numberVal;
		if (values.color !== undefined) {
			this.input_color.val(values.color);
		}
		if (values.size !== undefined) {
			this.input_size.val(values.size);
		} 
		if (values.number !== undefined) {
			this.input_number.val(values.number);
		} 
		
		console.log('color: ' + this.input_color.val() + ', sizeVal: ' + this.input_size.val() + ', numberVal: ' + this.input_number.val());
	}

	return pdContentUI;

})